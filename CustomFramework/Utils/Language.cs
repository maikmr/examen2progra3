﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CustomFramework.Resources;

namespace CustomFramework.Utils
{
    public static class Language
    {
        public static string For_the_field()
        {
            return LanguagePackage.For_the_field;
        }
        public static string Must_be_a_numeric_value()
        {
            return LanguagePackage.Must_be_a_numeric_value;
        }
        public static string Must_be_alphanumeric_value()
        {
            return LanguagePackage.Must_be_alphanumeric_value;
        }
        public static string The_maximum_length_is()
        {
            return LanguagePackage.The_maximum_length_is;
        }
        public static string The_minimum_length_is()
        {
            return LanguagePackage.The_minimum_length_is;
        }
        public static string Value_is_required()
        {
            return LanguagePackage.Value_is_required;
        }

    }
}
