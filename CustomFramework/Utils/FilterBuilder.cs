﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFramework.Utils
{
    public class FilterBuilder
    {
        private StringBuilder filter;

        public FilterBuilder()
        {
            this.filter = new StringBuilder();
        }

        private String EvalUnion()
        {
            return filter.ToString() == String.Empty ? String.Empty : " AND ";
        }

        public void AddStringFilter(string keyName, string keyValue, StringFilterOperator stringFilterOperator)
        {
            if (keyValue != string.Empty)
            {
                string valStart = StringFilterOperator.Equals == stringFilterOperator ? "'" : "'%";
                string valEnd = StringFilterOperator.Equals == stringFilterOperator ? "'" : "%'";
                filter.Append(EvalUnion() 
                    + keyName 
                    + CustomAttributes.GetDescriptionAttribute(stringFilterOperator) 
                    + valStart 
                    + keyValue 
                    + valEnd);
            }
        }

        public void AddNumericFilter(string keyName, string keyValue, NumericFilterOperator numberFilterOperator)
        {
            if (keyValue != string.Empty)
                filter.Append(EvalUnion() 
                    + keyName 
                    + CustomAttributes.GetDescriptionAttribute(numberFilterOperator) 
                    + keyValue);
        }

        public String GetFilter()
        {
            return filter.ToString();
        }
    }
}
