﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using CustomFramework.Resources;

namespace CustomFramework.Utils
{
    public class Validator
    {
        private Boolean _mustBeNumber;
        private Boolean _mustBeRequired;
        private Boolean _mustBeAlphanumeric;

        private Int32? _minimumLength;
        private Int32? _maximumLength;

        public Boolean IsValidate { get; set; }
        public String ErrorDescription { get; set; }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        public Validator(ValidationConstraint vc1)
        {
            AddValidationConstraint(vc1);
        }
        
        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        public Validator(ValidationConstraint vc1, int minimumLength)
        {
            AddValidationConstraint(vc1);
            this._minimumLength = minimumLength;
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        /// <param name="maximumLength">Establece longitud máxima</param>
        public Validator(ValidationConstraint vc1, int minimumLength, int maximumLength)
        {
            AddValidationConstraint(vc1);
            this._minimumLength = minimumLength;
            this._maximumLength = maximumLength;
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2, int minimumLength)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
            this._minimumLength = minimumLength;
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        /// <param name="maximumLength">Establece longitud máxima</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2,
            int minimumLength, int maximumLength)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
            this._minimumLength = minimumLength;
            this._maximumLength = maximumLength;
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        /// <param name="vc3">Tercera restricción</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2,
            ValidationConstraint vc3)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
            AddValidationConstraint(vc3);
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        /// <param name="vc3">Tercera restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2,
            ValidationConstraint vc3, int minimumLength)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
            AddValidationConstraint(vc3);
            this._minimumLength = minimumLength;
        }

        /// <summary>
        /// Instancia un nuevo objeto validador
        /// </summary>
        /// <param name="vc1">Primera restricción</param>
        /// <param name="vc2">Segunda restricción</param>
        /// <param name="vc3">Tercera restricción</param>
        /// <param name="minimumLength">Establece longitud mínima</param>
        /// <param name="maximumLength">Establece longitud máxima</param>
        public Validator(ValidationConstraint vc1, ValidationConstraint vc2,
            ValidationConstraint vc3, int minimumLength, int maximumLength)
        {
            AddValidationConstraint(vc1);
            AddValidationConstraint(vc2);
            AddValidationConstraint(vc3);
            this._minimumLength = minimumLength;
            this._maximumLength = maximumLength;
        }

        /// <summary>
        /// Agrega la restricción recibida por parámetro
        /// </summary>
        /// <param name="validationConstraint">Restricción para validación</param>
        private void AddValidationConstraint(ValidationConstraint validationConstraint)
        {
            switch (validationConstraint)
            {
                case ValidationConstraint.MustBeNumber:
                    this._mustBeNumber = true;
                    break;
                case ValidationConstraint.MustBeAlphanumeric:
                    this._mustBeAlphanumeric = true;
                    break;
                case ValidationConstraint.MustBeRequired:
                    this._mustBeRequired = true;
                    break;
            }
        }

        public Boolean Validate(string key, string value, int minimumLength)
        {
            this._minimumLength = minimumLength;
            return Validate(key, value);
        }

        public Boolean Validate(string key, string value, int minimumLength, int maximumLength)
        {
            this._minimumLength = minimumLength;
            this._maximumLength = maximumLength;
            return Validate(key, value);
        }

        public Boolean Validate(string key, string value)
        {
            ErrorDescription = String.Empty;
            IsValidate = true;

            //Validar que el valor no esté vacío (cuando el valor es requerido)
            if (_mustBeRequired)
            {
                if (value == String.Empty)
                {
                    this.ErrorDescription += " - " + LanguagePackage.Value_is_required + ".\n";
                    this.IsValidate = false;
                }
            }

            //Validar que el valor sea alfanumérico
            if (_mustBeAlphanumeric)
            {
                Regex alphanumericPattern = new Regex("^[0-9a-zA-Z]+$");
                if (!alphanumericPattern.IsMatch(value))
                {
                    this.ErrorDescription += " - " + LanguagePackage.Must_be_alphanumeric_value + ".\n"; 
                    IsValidate = false;
                }
            }

            //Validar que sea un número
            if (_mustBeNumber)
            {
                try
                {
                    int i = int.Parse(value);
                }
                catch (Exception)
                {
                    this.ErrorDescription += " - " + LanguagePackage.Must_be_a_numeric_value + "\n";
                    this.IsValidate = false;
                }
            }

            //Validar longitud mínima
            if (_minimumLength != null)
            {
                if (value.Length < _minimumLength)
                {
                    this.ErrorDescription += " - " + LanguagePackage.The_minimum_length_is + " "
                        + _minimumLength + ".\n";
                    this.IsValidate = false;
                }
            }

            //Validar longitud máxima
            if (_maximumLength != null)
            {
                if (value.Length > _maximumLength)
                {
                    this.ErrorDescription += " - " + LanguagePackage.The_maximum_length_is + " "
                        + _maximumLength + ".\n";
                    this.IsValidate = false;
                }
            }

            if (!IsValidate)
                this.ErrorDescription = "\n" + LanguagePackage.For_the_field + "\"" + key + "\":\n"
                    + this.ErrorDescription;

            return IsValidate;
        }
    }
}
