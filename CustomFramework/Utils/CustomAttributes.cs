﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFramework.Utils
{
    public static class CustomAttributes
    {
        public static string GetDescriptionAttribute(Enum val)
        {
            //Se obtiene el tipo de la enumeración
            Type type = val.GetType();

            //Se obtiene la información del elemento de la enumeración
            System.Reflection.FieldInfo fileInfo = type.GetField(val.ToString());

            //Se obtiene el atributo personalizado "Description", 
            //nótese que DescriptionAttribute es una clase de System.ComponentModel
            DescriptionAttribute[] attributes =
                fileInfo.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            //Se retorna cadena con el atributo, si no se encontró se retorna un vacío
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;        
        }
    }
}
