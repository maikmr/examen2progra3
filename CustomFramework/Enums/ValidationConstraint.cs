﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFramework.Utils
{
    public enum ValidationConstraint
    {
        /// <summary>
        /// Restricción valor numérico
        /// </summary>
        MustBeNumber,

        /// <summary>
        /// Restricción valor alfanumérico
        /// </summary>
        MustBeAlphanumeric,

        /// <summary>
        /// Restricción valor requerido
        /// </summary>
        MustBeRequired
    }

}
