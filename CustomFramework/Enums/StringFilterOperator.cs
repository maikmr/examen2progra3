﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFramework.Utils
{
    /// <summary>
    /// Operadores de consulta para condición de texto
    /// </summary>
    public enum StringFilterOperator
    {
        /// <summary>
        /// El operador Like permite buscar textos similares
        /// </summary>
        [Description(" like ")]
        Like,
        /// <summary>
        /// El operador Equal busca textos exactamente iguales
        /// </summary>
        [Description(" = ")]
        Equals
    }
}
