﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomFramework.Utils
{
    /// <summary>
    /// Operadores lógicos de consulta para condición numérica
    /// </summary>
    public enum NumericFilterOperator
    {
        /// <summary>
        /// Mayor que
        /// </summary>
        [Description(" > ")]
        GreaterThan,
        /// <summary>
        /// Menor que
        /// </summary>
        [Description(" < ")]
        LessThan,
        /// <summary>
        /// Igual que
        /// </summary>
        [Description(" = ")]
        EqualTo,
        /// <summary>
        /// Menor o igual que
        /// </summary>
        [Description(" <= ")]
        LessThanOrEqualTo,
        /// <summary>
        /// Mayor o igual que
        /// </summary>
        [Description(" >= ")]
        GreaterOrEqualTo
    }
}
