﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.34209
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CustomFramework.Resources {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class LanguagePackage {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LanguagePackage() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CustomFramework.Resources.LanguagePackage", typeof(LanguagePackage).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Para el campo.
        /// </summary>
        internal static string For_the_field {
            get {
                return ResourceManager.GetString("For_the_field", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Debe ser un valor numérico.
        /// </summary>
        internal static string Must_be_a_numeric_value {
            get {
                return ResourceManager.GetString("Must_be_a_numeric_value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Debe ser un valor alfanumérico, sin caracteres especiales.
        /// </summary>
        internal static string Must_be_alphanumeric_value {
            get {
                return ResourceManager.GetString("Must_be_alphanumeric_value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a La longitud máxima es.
        /// </summary>
        internal static string The_maximum_length_is {
            get {
                return ResourceManager.GetString("The_maximum_length_is", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a La longitud miníma es.
        /// </summary>
        internal static string The_minimum_length_is {
            get {
                return ResourceManager.GetString("The_minimum_length_is", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El valor es requerido.
        /// </summary>
        internal static string Value_is_required {
            get {
                return ResourceManager.GetString("Value_is_required", resourceCulture);
            }
        }
    }
}
