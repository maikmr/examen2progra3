﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Examen2.Models;

using MultiDataAccesFramework;
using MultiDataAccesFramework.Dictionaries;
using MultiDataAccesFramework.Resources;

using CustomFramework.Utils;
using Examen2.Classes;

namespace Examen2.Views
{
    public partial class MaintenanceFormSystems : Form
    {
        private Boolean isEditing = false;

        // Una variable auxiliar es necesaria cuando se va a permitir modificar la llave primaria
        // private String primaryKey;

        private DataView ExternalSystemsDataView;
        private Boolean isFiltered = false;
        private FilterBuilder filterBuilder;

        private DataAccess da;
        private DatabaseParameters dp;

        public MaintenanceFormSystems(DataAccess da, DatabaseParameters dp)
        {
            InitializeComponent();

            this.da = da;
            this.dp = dp;
        }
        
        private void MaintenanceFormSystems_Load(object sender, EventArgs e)
        {
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);

            dgExternalSystems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgExternalSystems.MultiSelect = false;
            cmbOwner.DropDownStyle = ComboBoxStyle.DropDownList;

            //Configurar el DataTable y asignarlo como origen de datos del DataGridView
            RefreshDataGridView();
            SetDataGridViewStyle();
        }

        private void RefreshDataGridView()
        {
            //Recuperar sistemas externos y verificar si ocurrió error en base de datos
            String sql = "SELECT "
                + ExternalSystem.TableName + "." + ExternalSystem.Fields.ID.GetName() + ", "
                + ExternalSystem.Fields.PersonID.GetName() + ", "
                + ExternalSystem.Fields.Description.GetName() + ", "
                + Person.Fields.Name.GetName() +  " || " + Person.Fields.Lastname.GetName() + " AS "
                + Person.Fields.Birthday.GetName()
                + " FROM "
                + dp.SchemaString + ExternalSystem.TableName
                + " JOIN " + dp.SchemaString + Person.TableName
                + " ON " + ExternalSystem.TableName + "." + ExternalSystem.Fields.PersonID.GetName()
                + " = " + Person.TableName + "." + Person.Fields.ID.GetName();
            // Para depurar el SQL descomentariar la siguiente linea
            // MessageBox.Show(sql);

            DataTable dtExternalSystems = da.ExecuteQuerySQL(sql);
            if (Utils.IsErrorOnDatabase(da))
                return;

            //Recuperar personas y verificar si ocurrió error en base de datos
            sql = "SELECT " 
                + Person.Fields.ID.GetName() + ", "
                + Person.Fields.Name.GetName() + " || ' ' || "
                + Person.Fields.Lastname.GetName() + " AS "
                + Person.Fields.Birthday.GetName() + " FROM "
                + dp.SchemaString + Person.TableName;
            // Para depurar el SQL descomentariar la siguiente linea
            // MessageBox.Show(sql);

            DataTable dtPersons = da.ExecuteQuerySQL(sql);
            if (Utils.IsErrorOnDatabase(da))
                return;

            if (isFiltered)
            {
                this.ExternalSystemsDataView = new DataView(dtExternalSystems);
                this.ExternalSystemsDataView.RowFilter = filterBuilder.GetFilter();
                this.dgExternalSystems.DataSource = this.ExternalSystemsDataView;
            }
            else
            {
                this.dgExternalSystems.DataSource = dtExternalSystems;
            }

            // Cargar el combobox a partir de la lista de usuarios
            this.cmbOwner.ValueMember = Person.Fields.ID.GetName();
            this.cmbOwner.DisplayMember = Person.Fields.Birthday.GetName();
            this.cmbOwner.DataSource = dtPersons;
            
            //this.cmbOwner.DataSource = Person.SyncDataTable();
            this.dgExternalSystems.Refresh();
        }

        private void SetDataGridViewStyle()
        {
            //Etiquetar las columnas del DataGridView
            DataTable dt = ExternalSystem.GetDataTableTemplate();
            foreach (DataGridViewColumn col in dgExternalSystems.Columns)
                col.HeaderText = dt.Columns[col.HeaderText].Caption;

            //Configurar ancho
            this.dgExternalSystems.Columns[ExternalSystem.Fields.ID.GetName()].Width = 100;
            this.dgExternalSystems.Columns[ExternalSystem.Fields.PersonID.GetName()].Width = 100;
            this.dgExternalSystems.Columns[ExternalSystem.Fields.Description.GetName()].Width = 170;
            this.dgExternalSystems.Columns[ExternalSystem.Fields.Fullname.GetName()].Width = 170;
        }

        private void ClearControls()
        {
            this.txtDescription.Text = string.Empty;
            this.txtDescription.Focus();
        }

        private void SettingToolbarButtons(
            NewButtonState newButtonState, EditButtonState editButtonState,
            SaveButtonState saveButtonState, CancelButtonState cancelButtonState,
            DeleteButtonState deleteButtonState, FindButtonState findButtonState,
            FilterButtonState filterButtonState, ReloadButtonState reloadButtonState,
            ExitButtonState exitButtonState)
        {
            this.tsbNew.Enabled = (newButtonState == NewButtonState.Enable ? true : false);
            this.tsbEdit.Enabled = (editButtonState == EditButtonState.Enable ? true : false);
            this.tsbSave.Enabled = (saveButtonState == SaveButtonState.Enable ? true : false);
            this.tsbCancel.Enabled = (cancelButtonState == CancelButtonState.Enable ? true : false);
            this.tsbDelete.Enabled = (deleteButtonState == DeleteButtonState.Enable ? true : false);
            this.tsbFind.Enabled = (findButtonState == FindButtonState.Enable ? true : false);
            this.tsbFilter.Enabled = (filterButtonState == FilterButtonState.Enable ? true : false);
        }

        private void ResetToolbar()
        {
            this.SettingToolbarButtons(
                NewButtonState.Enable, EditButtonState.Enable,
                SaveButtonState.Disable, CancelButtonState.Disable,
                DeleteButtonState.Enable, FindButtonState.Enable,
                FilterButtonState.Disable, ReloadButtonState.Enable,
                ExitButtonState.Enable);
        }

        private void SettingControls(StateOfControls stateOfControls)
        {
            Boolean state = (stateOfControls == StateOfControls.Enable ? true : false);
            this.txtDescription.Enabled = state;
            this.cmbOwner.Enabled = state;
            if (!isEditing)
                dgExternalSystems.ClearSelection();
            this.dgExternalSystems.Enabled = !state;
        }

        private Boolean isControlsOnValidState()
        {
            Validator valDescription = new Validator(ValidationConstraint.MustBeRequired);

            if (valDescription.Validate("Descripción",txtDescription.Text))
            {
                return true;
            }
            else
            {
                MessageBox.Show(valDescription.ErrorDescription,
                                "Error al validar campos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void LoadControls(int rowIndex)
        {
            if (rowIndex >= 0)
            {
                DataGridViewRow row = this.dgExternalSystems.Rows[rowIndex];
                if (row.Cells[0].Value != null)
                {
                    this.txtDescription.Text = this.dgExternalSystems
                        [CustomAttributes.GetDescriptionAttribute(ExternalSystem.Fields.Description),
                        dgExternalSystems.CurrentRow.Index].Value.ToString();

                    //Seleccionar en el combo el valor que corresponde a la fila
                    this.cmbOwner.SelectedValue = dgExternalSystems.CurrentRow.
                        Cells[ExternalSystem.Fields.PersonID.GetName()].Value;
                }
            }
        }

        private void dgExternalSystems_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.LoadControls(e.RowIndex);
        }


        private void tsbNew_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Enable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Disable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (dgExternalSystems.Rows.Count > 0 && dgExternalSystems.SelectedRows.Count > 0)
            {
                this.isEditing = true;

                // Cuando se permite la modificación de la llave primaria, se debe respaldar
                // antes de poner la pantalla en modo de edición
                // this.primaryKey = txtID.Text;

                SettingControls(StateOfControls.Enable);
                txtDescription.Focus();
                this.SettingToolbarButtons(
                    NewButtonState.Disable, EditButtonState.Disable,
                    SaveButtonState.Enable, CancelButtonState.Enable,
                    DeleteButtonState.Disable, FindButtonState.Disable,
                    FilterButtonState.Disable, ReloadButtonState.Disable,
                    ExitButtonState.Enable);
            }
            else
                MessageBox.Show("Seleccione la fila que desea editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbCancel_Click(object sender, EventArgs e)
        {
            this.isEditing = false;
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            if (isControlsOnValidState())
            {
                // Independientemente de que se vaya a hacer un "update" o un "insert" se
                // puede cargar en parámetros los que está en los controles de entrada

                ParametersForPostgreSQL pfp = new ParametersForPostgreSQL();

                pfp.Add(ExternalSystem.Fields.Description.GetName(),
                    txtDescription.Text, GenericParameterType.Varchar);

                pfp.Add(ExternalSystem.Fields.PersonID.GetName(),
                    cmbOwner.SelectedValue.ToString(), GenericParameterType.Integer);

                if (isEditing)
                {
                    // Si se está editando se necesita el valor actual de la llave primaria
                    pfp.Add(Person.Fields.ID.GetName(),
                        dgExternalSystems.SelectedCells[0].Value,
                        GenericParameterType.Integer);

                    da.ExecuteSQL("UPDATE " + dp.SchemaString + ExternalSystem.TableName +
                        " SET person_id = :person_id, description = :description " +
                        " WHERE id = :id", pfp.GetParametersArray());
                }
                else
                {
                    da.ExecuteSQL("INSERT INTO " + dp.SchemaString + ExternalSystem.TableName + " " +
                        "(person_id, description) VALUES" +
                        "(:person_id, :description)", pfp.GetParametersArray());
                }
                if (!Utils.IsErrorOnDatabase(da))
                    RefreshDataGridView();

                this.isEditing = false;
                this.ClearControls();
                this.ResetToolbar();
                SettingControls(StateOfControls.Disable);
            }
            else
            {
                this.txtDescription.Focus();
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (dgExternalSystems.Rows.Count > 0 && dgExternalSystems.SelectedRows.Count > 0)
            {
                string sql = 
                    "DELETE FROM " + dp.SchemaString + ExternalSystem.TableName
                    + " WHERE " + ExternalSystem.Fields.ID + " = "
                    + dgExternalSystems.SelectedCells[0].Value.ToString();

                // Para depurar SQL descomentariar la siguiente linea
                // MessageBox.Show(sql);

                da.ExecuteSQL(sql);

                if (Utils.IsErrorOnDatabase(da))
                    return;

                RefreshDataGridView();
                if (dgExternalSystems.Rows.Count <= 0)
                    ClearControls();
                this.ResetToolbar();
            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbFind_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Disable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Enable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        private void tsbFilter_Click(object sender, EventArgs e)
        {
            isFiltered = true;

            filterBuilder = new FilterBuilder();
            
            filterBuilder.AddStringFilter(
                ExternalSystem.Fields.Description.GetName(),
                txtDescription.Text, StringFilterOperator.Like);

            filterBuilder.AddNumericFilter(
                ExternalSystem.Fields.PersonID.GetName(),
                cmbOwner.SelectedValue.ToString(), NumericFilterOperator.EqualTo);

            this.RefreshDataGridView();
            this.ClearControls();
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbReload_Click(object sender, EventArgs e)
        {
            this.isFiltered = false;
            this.RefreshDataGridView();
            this.ResetToolbar();
            this.ClearControls();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
