﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Examen2.Models;
using Examen2.Classes;

using MultiDataAccesFramework;
using MultiDataAccesFramework.Dictionaries;
using MultiDataAccesFramework.Resources;

using CustomFramework.Utils;

namespace Examen2.Views
{
    public partial class MaintenanceFormPersons : Form
    {
        private Boolean isEditing = false;

        // Una variable auxiliar es necesaria cuando se va a permitir modificar la llave primaria
        // private String primaryKey;
        
        private DataView UsersDataView;
        private Boolean isFiltered = false;
        private FilterBuilder filterBuilder;

        private DataAccess da;
        private DatabaseParameters dp;

        public MaintenanceFormPersons(DataAccess da, DatabaseParameters dp)
        {
            InitializeComponent();

            this.da = da;
            this.dp = dp;
        }

        private void MaintenanceFormUsers_Load(object sender, EventArgs e)
        {
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);

            dgUsers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgUsers.MultiSelect = false;

            //Configurar el DataTable y asignarlo como origen de datos del DataGridView
            RefreshDataGridView();
            SetDataGridViewStyle();
        }

        private void RefreshDataGridView()
        {
            DataTable dt = da.ExecuteQuerySQL("SELECT * FROM " + dp.SchemaString + Person.TableName);

            //Verificar si ocurrió error en base de datos
            if (Utils.IsErrorOnDatabase(da))
                return;

            if (isFiltered)
            {
                this.UsersDataView = new DataView(dt);
                this.UsersDataView.RowFilter = filterBuilder.GetFilter();
                this.dgUsers.DataSource = this.UsersDataView;
            }
            else
            {
                this.dgUsers.DataSource = dt;
            }
            this.dgUsers.Refresh();
        }

        private void SetDataGridViewStyle()
        {
            //Etiquetar las columnas del DataGridView
            DataTable dt = Person.GetDataTableTemplate();
            foreach (DataGridViewColumn col in dgUsers.Columns)
                col.HeaderText = dt.Columns[col.HeaderText].Caption;
            
            //Configurar ancho
            //this.dgUsers.Columns[Person.Fields.ID.GetName()].Visible = false;
            //this.dgUsers.Columns[Person.Fields.DocumentID.GetName()].Width = 100;
            //this.dgUsers.Columns[Person.Fields.Name.GetName()].Width = 170;
            //this.dgUsers.Columns[Person.Fields.Lastname.GetName()].Width = 255;
            //this.dgUsers.Columns[Person.Fields.FullName)].Visible = false;
        }

        private void ClearControls()
        {
            this.txtDocumentID.Text = string.Empty;
            this.txtName.Text = String.Empty;
            this.txtLastname.Text = string.Empty;
            this.tBGender.Text = string.Empty;
            this.txtDocumentID.Focus();
        }

        private void SettingToolbarButtons(
            NewButtonState newButtonState, EditButtonState editButtonState,
            SaveButtonState saveButtonState, CancelButtonState cancelButtonState,
            DeleteButtonState deleteButtonState, FindButtonState findButtonState,
            FilterButtonState filterButtonState, ReloadButtonState reloadButtonState,
            ExitButtonState exitButtonState)
        {
            this.tsbNew.Enabled = (newButtonState == NewButtonState.Enable ? true : false);
            this.tsbEdit.Enabled = (editButtonState == EditButtonState.Enable ? true : false);
            this.tsbSave.Enabled = (saveButtonState == SaveButtonState.Enable ? true : false);
            this.tsbCancel.Enabled = (cancelButtonState == CancelButtonState.Enable ? true : false);
            this.tsbDelete.Enabled = (deleteButtonState == DeleteButtonState.Enable ? true : false);
            this.tsbFind.Enabled = (findButtonState == FindButtonState.Enable ? true : false);
            this.tsbFilter.Enabled = (filterButtonState == FilterButtonState.Enable ? true : false);
            this.tsbReload.Enabled = (reloadButtonState == ReloadButtonState.Enable ? true : false);
        }

        private void ResetToolbar()
        {
            this.SettingToolbarButtons(
                NewButtonState.Enable, EditButtonState.Enable,
                SaveButtonState.Disable, CancelButtonState.Disable,
                DeleteButtonState.Enable, FindButtonState.Enable,
                FilterButtonState.Disable, ReloadButtonState.Enable,
                ExitButtonState.Enable);
        }

        private void SettingControls(StateOfControls stateOfControls)
        {
            Boolean state = (stateOfControls == StateOfControls.Enable ? true : false);
            this.txtDocumentID.Enabled = state;
            this.txtName.Enabled = state;
            this.txtLastname.Enabled = state;
            this.dtpBirthday.Enabled = state;
            this.tBGender.Enabled = state;
            if (!isEditing)
                dgUsers.ClearSelection();
            this.dgUsers.Enabled = !state;
        }

        private Boolean isControlsOnValidState()
        {
            Validator valDocumentID = new Validator(ValidationConstraint.MustBeRequired,
                ValidationConstraint.MustBeAlphanumeric, 9, 18);

            Validator valName = new Validator(ValidationConstraint.MustBeRequired);

            Validator valLastname = new Validator(ValidationConstraint.MustBeRequired);

            if (valDocumentID.Validate("Identificación",txtDocumentID.Text) &
                valName.Validate("Nombre",txtName.Text) &
                valLastname.Validate("Apellido",txtLastname.Text))
            {
                return true;
            }
            else
            {
                MessageBox.Show(valDocumentID.ErrorDescription +
                                valName.ErrorDescription +
                                valLastname.ErrorDescription,
                                "Error al validar campos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        private void LoadControls(int rowIndex)
        {
            if (rowIndex >= 0)
            {
                DataGridViewRow row = this.dgUsers.Rows[rowIndex];
                if (row.Cells[0].Value != null)
                {
                    this.txtDocumentID.Text = this.dgUsers[Person.Fields.DocumentID.GetName(),
                        dgUsers.CurrentRow.Index].Value.ToString();

                    this.txtName.Text = this.dgUsers[Person.Fields.Name.GetName(),
                        dgUsers.CurrentRow.Index].Value.ToString();

                    this.txtLastname.Text = this.dgUsers[Person.Fields.Lastname.GetName(),
                        dgUsers.CurrentRow.Index].Value.ToString();

                    this.tBGender.Text = this.dgUsers[Person.Fields.Gender.GetName(),
                        dgUsers.CurrentRow.Index].Value.ToString();

                }
            }
        }

        private void dgUsers_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.LoadControls(e.RowIndex);
        }

        private void tsbNew_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Enable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Disable, ReloadButtonState.Disable, 
                ExitButtonState.Enable);
        }

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (dgUsers.Rows.Count > 0 && dgUsers.SelectedRows.Count > 0)
            {
                this.isEditing = true;

                // Cuando se permite la modificación de la llave primaria, se debe respaldar
                // antes de poner la pantalla en modo de edición
                // this.primaryKey = txtDocumentID.Text;

                SettingControls(StateOfControls.Enable);
                txtDocumentID.Focus();
                this.SettingToolbarButtons(
                    NewButtonState.Disable, EditButtonState.Disable,
                    SaveButtonState.Enable, CancelButtonState.Enable,
                    DeleteButtonState.Disable, FindButtonState.Disable,
                    FilterButtonState.Disable, ReloadButtonState.Disable,
                    ExitButtonState.Enable);
            }
            else
                MessageBox.Show("Seleccione la fila que desea editar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbCancel_Click(object sender, EventArgs e)
        {
            this.isEditing = false;
            ClearControls();
            ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            if (isControlsOnValidState())
            {
                // Independientemente de que se vaya a hacer un "update" o un "insert" se
                // puede cargar en parámetros los que está en los controles de entrada

                ParametersForPostgreSQL pfp = new ParametersForPostgreSQL();

                pfp.Add(Person.Fields.DocumentID.GetName(),
                    txtDocumentID.Text, GenericParameterType.Varchar);

                pfp.Add(Person.Fields.Name.GetName(),
                    txtName.Text, GenericParameterType.Varchar);

                pfp.Add(Person.Fields.Lastname.GetName(),
                    txtLastname.Text, GenericParameterType.Varchar);
                
                //mios
                pfp.Add(Person.Fields.Birthday.GetName(),
                    dtpBirthday.Value, GenericParameterType.Date);

                pfp.Add(Person.Fields.Gender.GetName(),
                    tBGender.Text, GenericParameterType.Integer);

                
                if (isEditing)
                {
                    // Si se está editando se necesita el valor actual de la llave primaria
                    pfp.Add(Person.Fields.ID.GetName(),
                        dgUsers.SelectedCells[0].Value,
                        GenericParameterType.Integer);

                    da.ExecuteSQL("UPDATE " + dp.SchemaString + Person.TableName +
                        " SET document = :document, first_name = :first_name, last_name = :last_name, birthday = :birthday, gender = :gender "
                      + " WHERE id_person = :id_person", pfp.GetParametersArray());
                }
                else
                {
                    da.ExecuteSQL("INSERT INTO " + dp.SchemaString + Person.TableName + " " +
                        "(document, first_name, last_name, birthday, gender) VALUES" +
                        "(:document, :first_name, :last_name, :birthday, :gender)", pfp.GetParametersArray());
                }
                if (!Utils.IsErrorOnDatabase(da))
                    RefreshDataGridView();

                this.isEditing = false;
                this.ClearControls();
                this.ResetToolbar();
                SettingControls(StateOfControls.Disable);
            }
            else
            {
                this.txtDocumentID.Focus();
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (dgUsers.Rows.Count > 0 && dgUsers.SelectedRows.Count > 0)
            {//" WHERE " + Person.Fields.ID + " = "
                string sql =
                    "DELETE FROM " + dp.SchemaString + Person.TableName
                    + " WHERE id_person = "
                    + dgUsers.SelectedCells[0].Value.ToString();

                // Para depurar SQL descomentariar la siguiente linea
                // MessageBox.Show(sql);

                da.ExecuteSQL(sql);

                if (Utils.IsErrorOnDatabase(da))
                    return;

                RefreshDataGridView();
                if (dgUsers.Rows.Count <= 0)
                    ClearControls();
                this.ResetToolbar();
            }
            else
                MessageBox.Show("Seleccione la fila que desea borrar", 
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void tsbFind_Click(object sender, EventArgs e)
        {
            SettingControls(StateOfControls.Enable);
            ClearControls();
            this.SettingToolbarButtons(
                NewButtonState.Disable, EditButtonState.Disable,
                SaveButtonState.Disable, CancelButtonState.Enable,
                DeleteButtonState.Disable, FindButtonState.Disable,
                FilterButtonState.Enable, ReloadButtonState.Disable,
                ExitButtonState.Enable);
        }

        private void tsbFilter_Click(object sender, EventArgs e)
        {
            isFiltered = true;

            filterBuilder = new FilterBuilder();

            filterBuilder.AddStringFilter(Person.Fields.DocumentID.GetName(),
                txtDocumentID.Text,
                StringFilterOperator.Like);

            filterBuilder.AddStringFilter(Person.Fields.Name.GetName(),
                txtName.Text,
                StringFilterOperator.Like);

            filterBuilder.AddStringFilter(Person.Fields.Lastname.GetName(),
                txtLastname.Text,
                StringFilterOperator.Like);

            this.RefreshDataGridView();
            this.ClearControls();
            this.ResetToolbar();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbReload_Click(object sender, EventArgs e)
        {
            this.isFiltered = false;
            this.RefreshDataGridView();
            this.ResetToolbar();
            this.ClearControls();
            SettingControls(StateOfControls.Disable);
        }

        private void tsbExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
} 