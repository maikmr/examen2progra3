﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Examen2.Views;
using Examen2.Models;

using MultiDataAccesFramework;

namespace Examen2
{
    public partial class frmMain : Form
    {
        private MaintenanceFormPersons maintenanceFormPersons;
        private MaintenanceFormSystems maintenanceFormSystems;

        private DataAccess _dataAccess;
        private DatabaseParameters _databaseParameters;

        public frmMain(DataAccess dataAccess, DatabaseParameters databaseParameters)
        {
            InitializeComponent();

            this._dataAccess = dataAccess;
            this._databaseParameters = databaseParameters;

        }

        private void miPersons_Click(object sender, EventArgs e)
        {
            try
            {
                if (maintenanceFormPersons.IsDisposed == true)
                    maintenanceFormPersons = new MaintenanceFormPersons(_dataAccess, _databaseParameters);
                else
                    maintenanceFormPersons.Activate();
            }
            catch (Exception) { maintenanceFormPersons = new MaintenanceFormPersons(_dataAccess, _databaseParameters); }

            maintenanceFormPersons.MdiParent = this;
            maintenanceFormPersons.ShowInTaskbar = false;
            maintenanceFormPersons.StartPosition = FormStartPosition.CenterScreen;
            maintenanceFormPersons.Show();
        }

        private void miSystems_Click(object sender, EventArgs e)
        {
            try
            {
                if (maintenanceFormSystems.IsDisposed == true)
                    maintenanceFormSystems = new MaintenanceFormSystems(_dataAccess, _databaseParameters);
                else
                    maintenanceFormSystems.Activate();
            }
            catch (Exception) { maintenanceFormSystems = new MaintenanceFormSystems(_dataAccess, _databaseParameters); }

            maintenanceFormSystems.MdiParent = this;
            maintenanceFormSystems.ShowInTaskbar = false;
            maintenanceFormSystems.StartPosition = FormStartPosition.CenterScreen;
            maintenanceFormSystems.Show();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
