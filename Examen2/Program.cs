﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using MultiDataAccesFramework;

namespace Examen2
{
    static class Program
    {
        static private DataAccess dataAccess;
        static private DatabaseParameters databaseParameters;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Se crea el objecto con los parámetros de conexión
            databaseParameters = new DatabaseParameters(
                "localhost", 5432, "examen2", "public", "postgres", "sa123456");

            // Se crea el objeto de conexión a base de datos (en este caso a PostgreSQL)
            dataAccess = new DataAccessForPostgreSQL(databaseParameters);

            // Se invoca el método que establece la conexión
            dataAccess.Connect();

            // Se verifican si ocurrieron errores
            if (dataAccess.IsError)
            {
                MessageBox.Show(dataAccess.ErrorDescription,
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain(dataAccess, databaseParameters));
            }
        }
    }
}
