﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace Examen2
{
    #region Buttons State Enums
    /// <summary>
    /// Estado del botón nuevo
    /// </summary>
    public enum NewButtonState
    {
        /// <summary>
        /// Botón "Nuevo" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Nuevo" deshabilitado
        /// </summary>
        Disable
    }
    public enum EditButtonState
    {
        /// <summary>
        /// Botón "Editar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Editar" deshabilitado
        /// </summary>
        Disable
    }
    public enum SaveButtonState
    {
        /// <summary>
        /// Botón "Guardar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Guardar" deshabilitado
        /// </summary>
        Disable
    }
    public enum CancelButtonState
    {
        /// <summary>
        /// Botón "Cancelar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Cancelar" deshabilitado
        /// </summary>
        Disable
    }
    public enum DeleteButtonState
    {
        /// <summary>
        /// Botón "Borrar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Borrar" deshabilitado
        /// </summary>
        Disable
    }
    public enum FindButtonState
    {
        /// <summary>
        /// Botón "Buscar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Buscar" deshabilitado
        /// </summary>
        Disable
    }
    public enum FilterButtonState
    {
        /// <summary>
        /// Botón "Filtrar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Filtrar" deshabilitado
        /// </summary>
        Disable
    }
    public enum ReloadButtonState
    {
        /// <summary>
        /// Botón "Recargar" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Recargar" deshabilitado
        /// </summary>
        Disable
    }
    public enum ExitButtonState
    {
        /// <summary>
        /// Botón "Salir" habilitado
        /// </summary>
        Enable,
        /// <summary>
        /// Botón "Salir" deshabilitado
        /// </summary>
        Disable
    }
    #endregion

    #region Control State Enums
    public enum StateOfControls
    { 
        /// <summary>
        /// Para definir el estado de los controles en "Habilitado"
        /// </summary>
        Enable,
        /// <summary>
        /// Para definir el estado de los controles de "Deshabilitado"
        /// </summary>
        Disable
    }
    #endregion
}
