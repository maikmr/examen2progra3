﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MultiDataAccesFramework;
using System.Windows.Forms;

namespace Examen2.Classes
{
    static class Utils
    {
        public static Boolean IsErrorOnDatabase(DataAccess da)
        {
            if (da.IsError)
            {
                MessageBox.Show(
                    da.ErrorDescription,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return true;
            }
            else
                return false;
        }
    }
}
