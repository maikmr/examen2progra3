﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.ComponentModel;
using CustomFramework.Utils;

namespace Examen2.Models
{
    class Person
    {
        //Atributos
        private int _id;
        private string _documentID;
        private string _name;
        private string _lastname;

        private const string tableName = "person";

        //Constructor
        public Person(string documentID, string name, string lastname)
        {
            this._documentID = documentID;
            this._name = name;
            this._lastname = lastname;
        }

        #region Properties
        public static string TableName
        {
            get { return tableName; }
        }
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string DocumentID
        {
            get { return _documentID; }
            set { _documentID = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }
        #endregion

        /// <summary>
        /// Enumeración con la definición de campos a utilizar en la vista
        /// </summary>
        public enum Fields
        {
            [Description("id_person")]
            ID,
            [Description("document")]
            DocumentID,
            [Description("first_name")]
            Name,
            [Description("last_name")]
            Lastname,
            [Description("birthday")]
            Birthday,
            [Description("gender")]
            Gender

        }

        /// <summary>
        /// En este método se define el modelo de la tabla
        /// </summary>
        /// <returns>DataTable con el modelo predefinido</returns>
        public static DataTable GetDataTableTemplate()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(Fields.ID.GetName(), typeof(int));
            dt.Columns.Add(Fields.DocumentID.GetName(), typeof(String));
            dt.Columns.Add(Fields.Name.GetName(), typeof(String));
            dt.Columns.Add(Fields.Lastname.GetName(), typeof(String));
            dt.Columns.Add(Fields.Birthday.GetName(), typeof(String));
            dt.Columns.Add(Fields.Gender.GetName(), typeof(int));


            dt.Columns[Fields.ID.GetName()].Caption = "ID";
            dt.Columns[Fields.DocumentID.GetName()].Caption = "Identificación";
            dt.Columns[Fields.Name.GetName()].Caption = "Nombre";
            dt.Columns[Fields.Lastname.GetName()].Caption = "Apellidos";
            dt.Columns[Fields.Birthday.GetName()].Caption = "Nacimiento";
            dt.Columns[Fields.Gender.GetName()].Caption = "Genero";
            return dt;
        }

    }
}
