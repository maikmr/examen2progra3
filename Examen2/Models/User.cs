﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.ComponentModel;
using CustomFramework.Utils;

namespace Examen2.Models
{
    class User
    {
        //Atributos
        private int _id;
        private int _personID;
        private string _username;
        private string _password;

        private const string tableName = "users";

        //Constructor
        public User(int id, int personID, string username, string password)
        {
            this._id = id;
            this._personID = personID;
            this._username = username;
            this._password = password;
        }

        #region Properties
        public string TableName
        {
            get { return tableName; }
        }
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int PersonID
        {
            get { return _personID; }
            set { _personID = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        #endregion

        /// <summary>
        /// Enumeración con la definición de campos a utilizar en la vista
        /// </summary>
        public enum Fields
        {
            [Description("id")]
            ID,
            [Description("person_id")]
            PersonID,
            [Description("username")]
            Username,
            [Description("password")]
            Password
        }

        /// <summary>
        /// En este método se define el modelo de la tabla
        /// </summary>
        /// <returns>DataTable con el modelo predefinido</returns>
        public static DataTable GetDataTableTemplate()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(Fields.ID.GetName(), typeof(int));
            dt.Columns.Add(Fields.PersonID.GetName(), typeof(int));
            dt.Columns.Add(Fields.Username.GetName(), typeof(String));
            dt.Columns.Add(Fields.Password.GetName(), typeof(String));

            dt.Columns[Fields.ID.GetName()].Caption = "ID";
            dt.Columns[Fields.PersonID.GetName()].Caption = "ID persona";
            dt.Columns[Fields.Username.GetName()].Caption = "Usuario";
            dt.Columns[Fields.Password.GetName()].Caption = "Contraseña";

            return dt;
        }

    }
}
