﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CustomFramework.Utils;

namespace Examen2.Models
{
    static class ModelsExtension
    {

        public static string GetName(this User.Fields val)
        {
            return CustomAttributes.GetDescriptionAttribute(val);
        }

        public static string GetName(this Person.Fields val)
        {
            return CustomAttributes.GetDescriptionAttribute(val);
        }

        public static string GetName(this ExternalSystem.Fields val)
        {
            return CustomAttributes.GetDescriptionAttribute(val);
        }

    }
}
