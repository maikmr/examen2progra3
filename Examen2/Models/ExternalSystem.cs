﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.ComponentModel;
using CustomFramework.Utils;

namespace Examen2.Models
{
    class ExternalSystem
    {
        //Atributos
        private int _id;
        private int _personID;
        private string _description;

        private const string tableName = "externalsystems";

        //Constructor
        public ExternalSystem(int id, int personID, string description)
        {
            this._id = id;
            this._personID = personID;
            this._description = description;
        }

        #region Propiedades
        public static string TableName
        {
            get { return tableName; }
        }
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public int PersonID
        {
            get { return _personID; }
            set { _personID = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        /// <summary>
        /// Enumeración con la definición de campos a utilizar en la vista
        /// </summary>
        public enum Fields
        {
            [Description("id")]
            ID,
            [Description("person_id")]
            PersonID,
            [Description("description")]
            Description,
            [Description("fullname")]
            Fullname
        }

        /// <summary>
        /// En este método se define el modelo de la tabla
        /// </summary>
        /// <returns>DataTable con el modelo predefinido</returns>
        public static DataTable GetDataTableTemplate()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(Fields.ID.GetName(), typeof(int));
            dt.Columns.Add(Fields.PersonID.GetName(), typeof(int));
            dt.Columns.Add(Fields.Description.GetName(), typeof(String));
            dt.Columns.Add(Fields.Fullname.GetName(), typeof(String));

            dt.Columns[Fields.ID.GetName()].Caption = "ID";
            dt.Columns[Fields.PersonID.GetName()].Caption = "ID propietario";
            dt.Columns[Fields.Description.GetName()].Caption = "Descripción";
            dt.Columns[Fields.Fullname.GetName()].Caption = "Propietario";

            return dt;
        }

    }
}
